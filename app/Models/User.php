<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticate;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticate
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $fillable = ['name', 'email', 'password', 'phone', 'address', 'avatar'];

    protected $hidden = ['password', 'remember_token'];

    public function messages()
    {
        return $this->hasMany(Message::class, 'chat_id');
    }

    public function sentMessages()
    {
        return $this->hasMany(Message::class, 'rep_id');
    }

    public function groups()
    {
        return $this->hasMany(Group::class,'user_id');
    }

    public function contacts()
    {
        return $this->hasOne(Contact::class, 'user_id');
    }

}
