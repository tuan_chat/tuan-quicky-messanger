<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ContactController extends Controller
{

    public function index()
    {
        $contacts = Contact::with('user')->get();
        $data = [];
        foreach ($contacts as $contact) {
            $data[] = [
                'user_id' => $contact->user_id,
                'name' => $contact->user->name,
            ];
        }
        return response()->json($data);
    }


    public function show($id)
    {
        $contact = Contact::with('user')->where('user_id',$id)->get();

        if (!$contact) {
            return response()->json(['error' => 'Thông tin liên hệ không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
        }

        return response()->json($contact);
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'status' => 'required',
        ]);

        $contact = Contact::create($request->all());

        return response()->json($contact, 201);
    }

    public function destroy($id){
        $contact = Contact::find($id);

        if (!$contact) {
            return response()->json(['error' => 'Nhóm không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
        }

        $contact->delete();

        return response()->json(['message' => 'Xóa nhóm thành công']);
    }
}
