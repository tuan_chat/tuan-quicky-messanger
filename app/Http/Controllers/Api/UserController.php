<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
//     show user
    public function index()
{
    $user = Auth::user();
    return response()->json($user);
}
//    public function index()
//    {
//        $loggedInUser = Auth::user();
//        $usersWithMessages = Message::where('user_id', $loggedInUser->id)->pluck('recipient_id');
//
//        $users = User::whereIn('id', $usersWithMessages)->get();
//
//        return response()->json($users);
//    }
    public function detail($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    public function show()
    {
        $users = User::all(['id', 'name']);

        return response()->json($users);
    }
//    public function show($id)
//    {
//        $user = User::find($id);
//
//        if (!$user) {
//            return response()->json(['error' => 'Thông tin người dùng không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
//        }
//
//        return response()->json($user);
//    }
    public function update(Request $request)
    {
        $user = Auth::user();
        $user->update([
            'name'=>$request->name,
            'phone_number'=>$request->phone_number,
            'address'=>$request->address,

        ]);
        return response()->json([
            'status'=>true,
            'message'=> 'Cập nhật thành công',
            'data'=>$user
        ]);
    }
    public function destroy(Request $request)
    {
        $user = $request->user();
        $user->delete();
        return response()->json([
            'status'=>true,
            'message'=> 'xóa thành công',
            'data'=>$user
        ]);
    }

}
