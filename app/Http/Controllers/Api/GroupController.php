<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;


class GroupController extends Controller
{
        public function index(){
        $groups = Group::with('user')->get();
        return response()->json($groups);
    }

        public function show($id)
    {
        $group = Group::where('user_id', Auth::user()->id)->find($id);

        if (!$group) {
            return response()->json(['error' => 'Nhóm không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
        }

        return response()->json($group);
    }



        public function store(Request $request){
        $request->validate([
            'message_content' => 'required',
        ]);
        $user = User::find($request->input('user_id'));
        $group = Group::create([
            'user_id' =>$user->id,
            'message_content' => $request->input('message_content'),


        ]);

        $group->save();

        return response()->json([
            $group,
            'user_name' => $user->name,
        ],201);
        }


        public function destroy($id){
        $group = Group::find($id);

        if (!$group) {
            return response()->json(['error' => 'Nhóm không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
        }

        $group->delete();

        return response()->json(['message' => 'Xóa nhóm thành công']);
    }
}
