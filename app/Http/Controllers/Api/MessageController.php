<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
//    public function index()
//    {
//        $messages = Message::with('user')->get();
//        return response()->json($messages);
//    }

    public function index()
    {
        $messages = Message::with('chat')->get();

        $messagesWithUserName = $messages->map(function ($message) {
            $userName = $message->chat->name;
            $message->user_name = $userName;
            unset($message->chat);
            return $message;
        });

        return response()->json($messagesWithUserName);
    }
    public function getUserMessages($repId)
    {
        $user = Auth::user();

        $messages = Message::where(function ($query) use ($user, $repId) {
            $query->where('chat_id', $user->id)
                ->where('rep_id', $repId);
        })->orWhere(function ($query) use ($user, $repId) {
            $query->where('chat_id', $repId)
                ->where('rep_id', $user->id);
        })->with('rep')->get();

        return response()->json($messages);
    }

    public function show($id)
    {
        $user = Auth::user();
        $message = Message::where(function ($query) use ($user, $id) {
            $query->where('chat_id', $user->id)
                ->orWhere('rep_id', $user->id)
                ->where('id', $id);
        })->first();

        if (!$message) {
            return response()->json(['error' => 'Tin nhắn không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
        }

        return response()->json($message);
    }

    public function store(Request $request, $repId)
    {
        $request->validate([
            'content' => 'required',
        ]);
        $user = User::findOrFail($repId); // Truy vấn người dùng dựa trên repId

        $message = Message::create([

            'chat_id' => Auth::id(), // Lấy id của người dùng hiện tại
            'rep_id' => $user->id,
            'content' => $request->input('content'),
//            'group_id' => $request->input('group_id')
        ]);

        return response()->json($message, 201);
    }
    public function destroy($id){
        $message =Message::find($id);

        if (!$message) {
            return response()->json(['error' => 'Nhóm không tồn tại hoặc không thuộc quyền sở hữu của bạn'], 404);
        }

        $message->delete();

        return response()->json(['message' => 'Xóa nhóm thành công']);
    }
}
