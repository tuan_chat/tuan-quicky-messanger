<?php

use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\GroupController;

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Register
Route::post('/register', [AuthController::class, 'createUser']);
Route::post('/login', [AuthController::class, 'loginUser']);
Route::post('/logout', [AuthController::class, 'logoutUser']);


Route::middleware('auth:sanctum')->group(function () {
    // User
    Route::prefix('user')->group(function () {
        Route::get('/', [UserController::class, 'index']);
        Route::get('/show', [UserController::class, 'show']);
        Route::get('/detail/{id}', [UserController::class, 'detail']);
        Route::post('/update', [UserController::class, 'update']);
        Route::delete('/delete', [UserController::class, 'destroy']);
    });
    // Contact// Đường dẫn: /api/contacts/{id}
    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', [ContactController::class, 'index']);
        Route::post('store', [ContactController::class, 'store']);
        Route::get('show/{id}', [ContactController::class, 'show']);
        Route::delete('delete/{id}', [ContactController::class, 'destroy']);
    });

    //group
    Route::group(['prefix' => 'group'], function () {
        Route::get('/', [GroupController::class, 'index']);
        Route::get('/show/{id}', [GroupController::class, 'show']);
        Route::post('/store', [GroupController::class, 'store']);
        Route::delete('delete/{id}', [GroupController::class, 'destroy']);
    });
        // message
    Route::group(['prefix' => 'message'], function () {
        Route::get('/', [MessageController::class, 'index']);
        Route::get('/show/{id}', [MessageController::class, 'show']);
        Route::get('/{repId}', [MessageController::class, 'getUserMessages']);
        Route::post('/store/{repId}', [MessageController::class, 'store']);
        Route::delete('delete/{id}', [MessageController::class, 'destroy']);
    });


});
