<?php
namespace Database\Factories;


use App\Models\Message;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => bcrypt('12345'), // Thay 'password' bằng mật khẩu mà bạn muốn sử dụng
            'phone' => $this->faker->phoneNumber,
            'address' => $this->faker->address,
            'avatar' => $this->faker->imageUrl(200, 200), // Đường dẫn ảnh đại diện
            'remember_token' => Str::random(10),
        ];
    }


}

