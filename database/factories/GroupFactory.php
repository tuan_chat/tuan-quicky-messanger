<?php


namespace Database\Factories;

use App\Models\Group;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class GroupFactory extends Factory
{
    protected $model = Group::class;

    public function definition()
    {
        $userIds = DB::table('users')->pluck('id')->toArray();

        return [
            'user_id' => $this->faker->randomElement($userIds),
            'message_content' => $this->faker->paragraph,
        ];
    }
}
