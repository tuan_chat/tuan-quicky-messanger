<?php

namespace Database\Factories;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class ContactFactory extends Factory
{
    public function definition()
    {
        $userIds = DB::table('users')->pluck('id')->toArray();

        return [
            'user_id' => $this->faker->randomElement($userIds),
            'status' => $this->faker->boolean,
        ];
    }
}
