<?php

namespace Database\Factories;

use App\Models\Group;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class MessageFactory extends Factory
{
    protected $model = Message::class;

    public function definition()
    {
        $userIds = DB::table('users')->pluck('id')->toArray();
        $groupIds = DB::table('groups')->pluck('id')->toArray();

        return [
            'chat_id' => $this->faker->randomElement($userIds),
            'rep_id' => $this->faker->randomElement($userIds),
            'group_id' => $this->faker->randomElement($groupIds),
            'content' => $this->faker->sentence,
        ];
    }
}
