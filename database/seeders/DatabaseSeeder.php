<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Group;
use App\Models\Message;
use Database\Factories\ContactFactory;
use Illuminate\Database\Seeder;
use App\Models\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */


    public function run()
    {
        User::factory()->count(50)->create();
        Contact::factory()->count(50)->create();
        Group::factory()->count(50)->create();
        Message::factory()->count(50)->create();



    }


}
